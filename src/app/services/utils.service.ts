import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Vacancy } from '../../app/classes/Vacancy';
import { Company } from '../../app/classes/Company';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  public currentCompanies: Company[];
  public currentVacancies: Vacancy[];
  public currentUser: any;

  
  constructor(
    private http: HttpClient
  ) { }

  public login(data): Observable<any>{
    let _params = 'email='+data.value.email+'&password='+data.value.password;
    var headers = new HttpHeaders('Content-Type: application/x-www-form-urlencoded');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
    var options = { headers: headers };
    
    return this.http.post("https://apidev.alanajobs.com/secure-candidate/login_check", _params, options)
               .pipe(map((response) => response));

  }

  public getCompanies(token): Observable<Company[]>{
    
    let _params =  {'offset': '0', 'limit': '10', 'apply': '0'};
    var headers = new HttpHeaders('Authorization: Bearer ' + token);
    var options = { params: _params, headers: headers };
    
    return this.http.get('https://apidev.alanajobs.com/secure-candidate/publication/company-index',  options)
               .pipe(
                    map((response) => {
                        let data = response['response'];
                        let companies: Company[] = [];
                        for (let item of data){
  
                            let company: Company = new Company();
                            company.idCompany = item.company.id;
                            company.photo = item.company.logo;
                            company.name = item.company.name;
                            company.numVacancies = item.jobs.length;
                            company.distance = item.maxDistance;
                            companies.push(company);
                        }
                         return companies;
                    })
                );
  }

  public getVacancies(token,id): Observable<Vacancy[]>{

    let _params =  {'offset': '0', 'limit': '10', 'apply': '0'};
    var headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token,
      'company': id.toString()
    });
    
    var options = {headers: headers, params: _params };
    
    return this.http.get('https://apidev.alanajobs.com/secure-candidate/publication/index', options)
              .pipe(
                    map((response) => {
                        let data = response['response'];
                        let vacancies: Vacancy[] = [];
                        for (let item of data){
                            
                            let vacancy: Vacancy = new Vacancy();
                             vacancy.uid = item.publication.uid;
                             vacancy.photo = item.publication.jobPosition.icon;
                             vacancy.jobPositionName = item.publication.jobPosition.jobPositionName;
                             vacancy.title = item.publication.title;
                             vacancy.distance = item.distance;
                             vacancy.mission = item.publication.mission;
                             vacancy.experience = item.publication.experience;
                             vacancy.nameCompany = item.name;
                             vacancy.addressCompany = item.addressName;
                             vacancy.photoCompany = item.logo;
                             vacancy.maxSalary = item.publication.maxSalary;
                             vacancy.minSalary = item.publication.minSalary;
                             vacancy.requirements = item.publication.requirements;
                             vacancies.push(vacancy);
                        }
                        this.currentVacancies = vacancies;
                        return vacancies;
                    })
                );

  }

  public getVacancyDetail(_uid): Vacancy{
    // let vacancies = this.currentVacancies;

    return this.currentVacancies.find(c => c.uid == _uid);

  }
}
