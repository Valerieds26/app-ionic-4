export class Vacancy {

    private _uid: string;
    get uid(): string { return this._uid };
    set uid(_uid: string) { this._uid = _uid };
    
    private _photo: string;
    get photo(): string { return this._photo };
    set photo(_photo: string) { this._photo = _photo };

    private _jobPositionName: string;
    get jobPositionName(): string { return this._jobPositionName };
    set jobPositionName(_jobPositionName: string) { this._jobPositionName = _jobPositionName };

    private _title: string;
    get title(): string { return this._title };
    set title(_title: string) { this._title = _title };

    private _distance: number;
    get distance(): number { return this._distance };
    set distance(_distance: number) { this._distance = _distance };

    private _mission: string;
    get mission(): string { return this._mission };
    set mission(_mission: string) { this._mission = _mission };

    private _experience: number;
    get experience(): number { return this._experience };
    set experience(_experience: number) { this._experience = _experience };

    private _requirements: any;
    get requirements(): any { return this._requirements };
    set requirements(_requirements: any) { this._requirements = _requirements };

    private _nameCompany: string;
    get nameCompany(): string { return this._nameCompany };
    set nameCompany(_nameCompany: string) { this._nameCompany = _nameCompany };

    private _photoCompany: string;
    get photoCompany(): string { return this._photoCompany };
    set photoCompany(_photoCompany: string) { this._photoCompany = _photoCompany };

    private _addressCompany: string;
    get addressCompany(): string { return this._addressCompany };
    set addressCompany(_addressCompany: string) { this._addressCompany = _addressCompany };

    private _maxSalary: string;
    get maxSalary(): string { return this._maxSalary };
    set maxSalary(_maxSalary: string) { this._maxSalary = _maxSalary };

    private _minSalary: string;
    get minSalary(): string { return this._minSalary };
    set minSalary(_minSalary: string) { this._minSalary = _minSalary };

    constructor() {

    }
}