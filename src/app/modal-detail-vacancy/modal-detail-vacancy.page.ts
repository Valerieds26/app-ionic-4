import { Component, OnInit, Input } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { Vacancy } from '../classes/Vacancy';

@Component({
  selector: 'app-modal-detail-vacancy',
  templateUrl: './modal-detail-vacancy.page.html',
  styleUrls: ['./modal-detail-vacancy.page.scss'],
})
export class ModalDetailVacancyPage {

  @Input() uid: string;
  public vacancy: Vacancy;


  constructor(
    private utilService:UtilsService, 
    private modalCtrl:ModalController,
    private navCtrl: NavController,
    navParams: NavParams)
  {
  
    this.vacancy = this.utilService.getVacancyDetail(navParams.get('uid'));
    console.log(this.vacancy);
  }


  public backPage(): void{
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
