import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../services/utils.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage'


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  public formData: FormGroup;
  private loader:any;
  public userData: any;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public modalCtrl:ModalController,
    public utilService: UtilsService,
    public loadingCtrl:LoadingController,
    public alertCtrl: AlertController,
    private router: Router,
    private storage: Storage) {

    this.formData = this.formBuilder.group({
      email: ['',[Validators.required, Validators.email]],
      password: ['',[Validators.required]],
      onRemember: [false]
    });

  }


  ionViewWillEnter(){
    this.storage.get('email').then((email) => {
      if(email){
        this.formData.patchValue({email: email});
        this.formData.patchValue({onRemember: true});
      }
    });
  }

  public login(): void{

    this.presentLoader('Espere un momento, iniciando sesión...');
    this.utilService.login(this.formData)
    .subscribe((response)=>{
      this.leaveLoader();
      this.storage.set('token', response['token']);
      if(this.formData.value.onRemember){
        this.storage.set('email',this.formData.value.email);
      }else{
        this.storage.remove('email');
        this.formData.patchValue({email: ''});
      }
      this.navCtrl.navigateRoot('/home');
    },err=>{
      console.log(err);
      this.leaveLoader();
      if(err.status == 401){
        this.showAlert();
      }
    });

  }


  private showAlert(): void {
    this.alertCtrl.create({
      subHeader: 'Error!',
      message: "Email o contraseña inválida.",
      buttons: ['OK']
    }).then(alert => alert.present());
  }

  private presentLoader(msj): void {
    this.loadingCtrl.create({
      message: msj,
    }).then(loader => {this.loader = loader;
      loader.present();
    });
  }
  
  private leaveLoader(): void{
    this.loader.dismiss();
  }

}
