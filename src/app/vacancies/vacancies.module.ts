import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VacanciesPage } from './vacancies.page';
import { AlanaItemsModule } from '../components/alana-items/alana-items.module';
import { ModalDetailVacancyPageModule } from '../modal-detail-vacancy/modal-detail-vacancy.module';
import { ModalDetailVacancyPage } from '../modal-detail-vacancy/modal-detail-vacancy.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlanaItemsModule,
    ModalDetailVacancyPageModule,

    RouterModule.forChild([
    {
      path: '',
      component: VacanciesPage
    },
  ],
  )],
  declarations: [VacanciesPage],
  entryComponents: [ModalDetailVacancyPage]
})
export class VacanciesPageModule {}
