import { Component, OnInit } from '@angular/core';
import { NavController,ModalController } from '@ionic/angular';
import { Vacancy } from '../../app/classes/Vacancy';
import { UtilsService } from '../services/utils.service';
import { ModalDetailVacancyPage } from '../modal-detail-vacancy/modal-detail-vacancy.page';

@Component({
  selector: 'app-vacancies',
  templateUrl: './vacancies.page.html',
  styleUrls: ['./vacancies.page.scss'],
})
export class VacanciesPage implements OnInit {

  vacancies: Vacancy[];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private utilService: UtilsService
  ) {
    this.vacancies = [];

  }


  ionViewWillEnter(){
    this.vacancies=this.utilService.currentVacancies;
  }

  ionViewWillLeave(){
  }

  ngOnInit() {
  }

  public async showDetailsVacancy(vacancy: Vacancy){
    const modal = await this.modalCtrl.create({
      component: ModalDetailVacancyPage,
      componentProps: {
        'uid': vacancy.uid,
      }
    });
    return await modal.present();
  }

  public backHome(): void{
    this.navCtrl.navigateRoot('/home');
  }

}
