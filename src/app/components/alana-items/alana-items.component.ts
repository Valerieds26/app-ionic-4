import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Company } from 'src/app/classes/Company';
import { Vacancy } from 'src/app/classes/Vacancy';

@Component({
  selector: 'app-alana-items',
  templateUrl: './alana-items.component.html',
  styleUrls: ['./alana-items.component.scss'],
})
export class AlanaItemsComponent implements OnInit {


  @Input('company') company: Company;
  @Input('vacancy') vacancy: Vacancy;
  @Output("action") companyEvent = new EventEmitter<boolean>(); 

  ngOnInit() { }
  
  public action(): void{
    this.companyEvent.emit(true);
  }

}
