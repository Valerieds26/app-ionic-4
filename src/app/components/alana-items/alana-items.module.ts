import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { AlanaItemsComponent } from './alana-items.component';


@NgModule({
  declarations: [
    AlanaItemsComponent,
  ],
  exports: [
    AlanaItemsComponent
  ],
  imports: [
      IonicModule
  ]
})
export class AlanaItemsModule {}