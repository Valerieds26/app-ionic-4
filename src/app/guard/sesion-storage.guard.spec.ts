import { TestBed, async, inject } from '@angular/core/testing';

import { SesionStorageGuard } from './sesion-storage.guard';

describe('SesionStorageGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SesionStorageGuard]
    });
  });

  it('should ...', inject([SesionStorageGuard], (guard: SesionStorageGuard) => {
    expect(guard).toBeTruthy();
  }));
});
