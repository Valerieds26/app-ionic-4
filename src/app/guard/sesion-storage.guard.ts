import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class SesionStorageGuard implements CanActivate {

    constructor( public navCtrl: NavController, public storage: Storage) {}
  
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      
      return new Promise<boolean>((resolve, reject) =>{
        this.storage.get('token').then((token) => {
          if (token) {
            this.navCtrl.navigateRoot('/home');
            resolve(false);
          } else {
            resolve(true);
          }
        },err=>{
          console.log(err);
          reject(false);
        });
      });
    }


}
