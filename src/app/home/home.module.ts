import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AlanaItemsModule } from '../components/alana-items/alana-items.module';
import { SesionStorageGuard } from '../guard/sesion-storage.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlanaItemsModule,
    RouterModule.forChild([
    {
      path: '',
      component: HomePage
    },
      { path: 'vacancies',loadChildren: () => import('../vacancies/vacancies.module').then( m => m.VacanciesPageModule) },

    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
