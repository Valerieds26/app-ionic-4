import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { Company } from '../classes/Company'; 
import { Vacancy } from '../classes/Vacancy';
import { UtilsService } from '../services/utils.service';
import { Storage } from '@ionic/storage'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private loader: any;
  public companies: Company[];
  public vacancies: Vacancy[];
  public token: string;

  constructor(
    private router: Router, 
    private loadingCtrl: LoadingController,
    private utilService: UtilsService,
    private navCtrl: NavController,
    private storage: Storage
    ){
      this.companies = [];
      this.vacancies = [];
    }

    ionViewWillEnter() {

      if(!this.utilService.currentCompanies){
        this.presentLoader('Espere un momento, cargando información...');
        this.getCompanies();
      }else{
        this.companies = this.utilService.currentCompanies;
        this.getCompanies();
      }
  } 

  

  public getVacancies(company: Company): void{
    this.presentLoader('Espere un momento, cargando información...');
      this.utilService.getVacancies(this.token,company.idCompany).subscribe(response =>{
        this.leaveLoader();
        this.vacancies = response;
        this.navCtrl.navigateRoot('/home/vacancies');
      },err=>{
        this.leaveLoader();
        console.log(err);
      });

  }

  private getCompanies(): void{
    this.storage.get('token').then((token) => { 
      this.token = token;         
      this.utilService.getCompanies(this.token).subscribe(response =>{
        if(!this.utilService.currentCompanies){this.leaveLoader();}
        this.companies = response;
        for ( let item of this.companies ){ 
          item.distance = Math.round(item.distance);
        }
        this.utilService.currentCompanies = this.companies;

      },err=>{
        if(!this.utilService.currentCompanies){this.leaveLoader();}
        console.log(err);
      });
    });
  }

  private presentLoader(msj): void {
    this.loadingCtrl.create({
      message: msj,
    }).then(loader => {this.loader = loader;
      loader.present();
    });
  }
  
  private leaveLoader(): void{
    this.loader.dismiss();
  }


}
